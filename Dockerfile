

FROM tensorflow/tensorflow:nightly-gpu-jupyter
USER root
WORKDIR /mnt/henayasta/



RUN apt-get update

RUN apt-get install python3-pydot -y

RUN pip install opencv-python

RUN apt-get install ffmpeg libsm6 libxext6 git  -y

RUN pip install -U segmentation-models

RUN pip install albumentations
